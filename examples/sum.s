#this program comes from http://www.ibm.com/developerworks/linux/library/l-powasm1/index.html
#Data sections holds writable memory declarations
.data
.align 3  #align to 8-byte boundary

#This is where we will load our first value from
#First value is constant not a variable! It's address of the first long variable
#long is 32 bit wide
first_value:
        .long 1
second_value:
        .long 2

#Switch to ".text" section for program code
.text
    .global _start
_start:
        #Use register 7 to load in an address
        #32-bit addresses must be loaded in 16-bit pieces

        lis 7, first_value@ha
        addi  7, 7, first_value@l

        #Load in first value to register 4, from the address we just loaded
        lwz 4, 0(7)

        #Load in the address of the second value
        lis 7, second_value@ha
        addi 7, 7, second_value@l

        #Load in the second value to register 5, from the address we just loaded
        lwz 5, 0(7)

        #Calculate the value and store into register 6
        add 6, 4, 5

        #Exit with the status
        li 0, 1    #system call is in register 0
        mr 3, 6    #Move result into register 3 for the system call

        sc
